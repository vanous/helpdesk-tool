function onInit(){

document.getElementById("button").addEventListener("click",storeCredentials);

function storeCredentials() {
    var username = document.getElementById("Cname").value;
    var password = document.getElementById("Cpass").value;
    chrome.storage.sync.set({'credentials':{'username':username,'password':password}}, function () {
        console.log("Just stored credentials")
		location.reload();
    });
}



function clearCredentials() {
		chrome.storage.sync.remove('credentials',function(){
		location.reload();
        console.log("Just cleared credentials")
    });
}
//document.getElementById("reenter").addEventListener("click",clearCredentials);




function loadCredentials() {
    chrome.storage.sync.get('credentials', function (result) {
        //credentials = result.channels;
        //alert(result.credentials);
		//var vysledky = JSON.stringify(result);
		var userC = result["credentials"].username
		var passC = result["credentials"].password;
		
	var ser = window.location.hash.replace("#","").split("#");
		
	var pole = document.getElementById('container');

		var ikona = chrome.extension.getURL('loading.gif');

		pole.innerHTML = '<img src='+ikona+' width=300 style="position:relative;left:100px;top:1px">';
		
		//var xmlHttp = new XMLHttpRequest();
		var theUrl = "https://app:user@spares.robe.cz:/"+ser[0]
		var req = new digestAuthRequest('GET', theUrl, userC, passC);
		// make the request
		//xmlHttp.open( "GET", theUrl, false );
		//xmlHttp.send( null );

	// make the request
	req.request(function(data) { 
		//var radky= xmlHttp.responseText.split("\n");
		var info = data.info[0]
		pole.innerHTML = "<h2>Serial number details:</h2>";

		pole.innerHTML += "<ul>";
		pole.innerHTML += "<li>Serial: "+info.serial;
		pole.innerHTML += "<li>Product: "+info.desc;
		pole.innerHTML += "<li>RDM UID: "+info.rdm;
		pole.innerHTML += "<li>MAC: " + info.mac;
		pole.innerHTML += "<li>Software: "+info.nav;
		
		var dat = info.date.split(" ")[0];
		if (dat ==="1753-01-01"){
			dat = "n/a"
			}

		pole.innerHTML += "<li>Manufactured (Y/M/D): " + dat;
		
		
		pole.innerHTML += "<li>Notes I: " + info.changes;
		pole.innerHTML += "<li>Notes II: " + info.notes;
		pole.innerHTML += "</ul>";
		
		calibrations=data.calibrations;
		if (calibrations.length > 0)
		{
		pole.innerHTML += "<li>Calibrations:";
		window.resizeTo(540, 320+(30*(calibrations.length+3)));

		}
		var k2700=false;
		var k4200=false;
		var k5500=false;
		var k5600=false;
		var k7800=false;
		var tungsten = "";
		
		for (i=0; i<calibrations.length;i++)
		{
		var casti = calibrations[i];
		
		switch(casti.temperature){
			
		case "2700":

			if (k2700)
			{
			pole.innerHTML+="<br>";				
			pole.innerHTML+="<li>"+ casti.date;				
			}else{
			pole.innerHTML+="<li>"+ casti.date;				
			k2700=true;
			}

			break;

		case "4200":
			if (k4200)
			{
				tungsten=" Theatre";
				k4200=false;		
			
			}else{

				k4200=true;			
			
			}

			break;

		case "5500":
			if (k5500 || k5600)
			{
				tungsten=" Theatre";
				k5500=false;		
				k5600=false;
			
			}else{

				k5500=true;			
				k5600=true;
			
			}

			break;

		case "5600":
			if (k5500 || k5600)
			{
				tungsten=" Theatre";
				k5500=false;
				k5600=false;
			
			}else{

				k5600=true;
				k5500=true;
			
			}

			break;

		case "7800":
			if (k7800)
			{
				tungsten=" Theatre";
				k7800=false;		
			
			}else{

				k7800=true;			
			
			}

			break;


		}

		pole.innerHTML += "<li>" + casti.temperature + tungsten + ": R: " + casti.red + ", G: " + casti.green + ", B: " + casti.blue + ", W: " + casti.white;
		var last_temp=casti.temperature;
		tungsten="";

		}



		changes=data.changes;
		console.log(changes)
		if (changes.length > 0)
		{
		pole.innerHTML += "<li>Changes:";
		window.resizeTo(540, 320+(30*(changes.length+3)));

		}
		
		for (i=0; i<changes.length;i++)
		{
		

		pole.innerHTML += "<li>" + changes[i].change_name + "("+changes[i].change_list_number +"): " + changes[i].nav_number + " " + changes[i].desc + " " + changes[i].nav_amount + "x";

		}

	  // success callback
	  // do something with the data
	},function(errorCode) {
	console.log("auth error",errorCode)
	
		if (errorCode===401)
		{
		console.log("clear credentials")
		//auth problem
		chrome.storage.sync.remove('credentials',function(){})
		location.reload();
		
		}else{
			pole.innerHTML = "<h2>Error</h2>" + errorCode;
			}
	  // error callback
	  // tell user request failed
		});
	});
}

loadCredentials()

}

document.addEventListener('DOMContentLoaded', onInit, false);
