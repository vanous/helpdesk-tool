var first_run=true;

var pageMod = require("sdk/page-mod");
var self = require("sdk/self");
pageMod.PageMod({
  include: "https://robelighting.jitbit.com/helpdesk/*",
contentScriptFile: "./myscript.js",
contentScriptOptions: {
pixUrl: self.data.url("pix.gif"),
iconUrl: self.data.url("info-icon.png")
},
onAttach: function(worker) {
    worker.port.on("message", function(elementContent) {

	if (first_run){
	windows.open(elementContent,'_blank', 'resizable=yes, scrollbars=yes, titlebar=no, width=520, height=320, top=10, left=10,location=0')
	first_run=false;
	
	}else{
	panel.contentURL=elementContent;
	panel.show();
      console.log(elementContent);
	  }
    });
	}

});

var panel = require("sdk/panel").Panel({
  width: 320,
  height: 520,
  contentURL: self.data.url("window.html"),
	position: {
    left: 40,
    top: 40
  }

});

var windows = require("sdk/windows").browserWindows;

panel.on('hide', function() {
  panel.contentURL = self.data.url("window.html")
  console.log("panel is hiding");
});

panel.on('show', function() {
  console.log("panel is showing");
});
